require 'rubygems'
require 'selenium-webdriver'

class LogIn
	def initialize( login, pass )
		@login = login
		@pass = pass
	end

	def set_profile_photo
		# Экземпляр браузера Firefox
		driver = Selenium::WebDriver.for :firefox
		# Экземпляр браузера Chrome
		# driver = Selenium::WebDriver.for :chrome

		# Загрузка Facebook
		driver.get "https://facebook.com"

		# Путь к файлу
		filename = 'smile.png'
  		file = File.join(Dir.pwd, filename)

		# Переменная ожидания 10 секунд
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)
		
		# Ввод email
		driver.find_element(:id, "email").send_keys @login
		# Ввод пароля
		driver.find_element(:id, "pass").send_keys @pass
		# Нажатие кнопки входа
		driver.find_element(:id, "loginbutton").click

		# Нажатие "Добавить фото/видео"
		wait.until { driver.find_element(:id, "js_1").send_keys file }
		
		# Выход из браузера
		driver.quit

		puts "Фото опубликовано"
	end
end

# Данные для авторизации вставляются из файла (первая строка - логин, вторая - пароль)
arr = Array.new
i = 0
File.open("credentials.txt", "r") do |f|
	f.each_line do |line|
		arr[i] = line
		i+=1
	end
end

login = LogIn.new(arr[0],arr[1])
login.set_profile_photo